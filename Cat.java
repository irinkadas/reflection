import java.lang.reflect.Field;

public class Cat {
    public static void main(String[] args) {

        Animal animal = new Animal("Tiger", "In India ", 5);
        Class<? extends Animal> animalClass = animal.getClass();
        Field kindOfAnimal;
        {
            try {
                kindOfAnimal = animalClass.getField("kindOfAnimal");
                System.out.println("Before change: " + kindOfAnimal.get(animal));
                kindOfAnimal.setAccessible(true);
                kindOfAnimal.set(animal, " Cat ");
                System.out.println("After change: " + kindOfAnimal.get(animal));
            }catch (NoSuchFieldException | IllegalAccessException a ) {
                throw new RuntimeException(a);
            }
        }
        Field placeOfResidence;
        {
            try {
                placeOfResidence = animalClass.getField("placeOfResidence");
                System.out.println("Before change: " + placeOfResidence.get(animal));
                placeOfResidence.set(animal, "At home");
                System.out.println("After change: " + placeOfResidence.get(animal));
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        Field age;
        {
            try {
                age = animalClass.getDeclaredField("age");
                age.setAccessible(true);
                System.out.println("Before change: " + age.getInt(animal));
                age.setInt(animal, 1);
                System.out.println("After change: " + age.getInt(animal));
            }catch (NoSuchFieldException | IllegalAccessException b) {
                throw new RuntimeException(b);
            }
        }

    }
}
