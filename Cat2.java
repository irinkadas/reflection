import java.lang.reflect.Field;
import java.util.Scanner;

public class Cat2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number, which class do you want to view animal 1 or cat 2? ");
        Animal animal = new Animal("Tiger", "In India ", 5);
        int number = scanner.nextInt();
        if(number == 1){
            Class<? extends Animal> animalClass = animal.getClass();
            Field kindOfAnimal;
            {
                try {
                    kindOfAnimal = animalClass.getField("kindOfAnimal");
                    System.out.println("Kind of animal: " + kindOfAnimal.get(animal));
                    kindOfAnimal.setAccessible(true);
                } catch (NoSuchFieldException | IllegalAccessException a) {
                    throw new RuntimeException(a);
                }
            }
            Field placeOfResidence;
            {
                try {
                    placeOfResidence = animalClass.getField("placeOfResidence");
                    System.out.println("Place of residence  " + placeOfResidence.get(animal));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            Field age;
            {
                try {
                    age = animalClass.getDeclaredField("age");
                    age.setAccessible(true);
                    System.out.println("Age: " + age.getInt(animal));
                } catch (NoSuchFieldException | IllegalAccessException b) {
                    throw new RuntimeException(b);
                }
            }
        }if(number == 2) {
            Class<? extends Animal> animalClass = animal.getClass();
            Field kindOfAnimal;
            {
                try {
                    kindOfAnimal = animalClass.getField("kindOfAnimal");
                    System.out.println("Before change - " + " kind of animal: " + kindOfAnimal.get(animal));
                    kindOfAnimal.setAccessible(true);
                    kindOfAnimal.set(animal, " Cat ");
                    System.out.println("After change - " + " kind of animal: " + kindOfAnimal.get(animal));
                } catch (NoSuchFieldException | IllegalAccessException a) {
                    throw new RuntimeException(a);
                }
            }
            Field placeOfResidence;
            {
                try {
                    placeOfResidence = animalClass.getField("placeOfResidence");
                    System.out.println("Before change - " + " place of residence: " + placeOfResidence.get(animal));
                    placeOfResidence.set(animal, "At home");
                    System.out.println("After change - " + " place of residence: " + placeOfResidence.get(animal));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            Field age;
            {
                try {
                    age = animalClass.getDeclaredField("age");
                    age.setAccessible(true);
                    System.out.println("Before change - " + " age: " + age.getInt(animal));
                    age.setInt(animal, 1);
                    System.out.println("After change - " + " age: " + age.getInt(animal));
                } catch (NoSuchFieldException | IllegalAccessException b) {
                    throw new RuntimeException(b);
                }
            }
        }
    }
}
