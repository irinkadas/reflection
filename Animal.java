public class Animal {
    public final String kindOfAnimal;
    public String placeOfResidence;
    private int age;


    public Animal(String kindOfAnimal, String placeOfResidence, int age) {
        this.kindOfAnimal = kindOfAnimal;
        this.placeOfResidence = placeOfResidence;
        this.age = age;
    }
}
